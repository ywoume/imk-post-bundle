<?php

namespace ImkPostBundle\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ImkPostBundle\Repository\PostRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PostRepository::class)
 */
#[ApiResource]
class Post
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $reference;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $titlePost;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private string $jobDescription;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private string $mission;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $skills;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private string $technos;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private string $services;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private string $tools;

    /**
     * @ORM\Column(type="text")
     */
    private string $profilRechercher;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private string $interview;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $contract;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTimeInterface $contractStartedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private \DateTimeInterface $contractEndedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private string $localisation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $workType;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private bool $isTransport;

    /**
     * @ORM\Column(type="text")
     */
    private string $diplomas;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private int $salary;

    /**
     * @ORM\Column(type="text")
     */
    private string $experience;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTimeInterface $updatedAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private bool $isTreated;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isFromPortal;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isFromEmploi;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTimeInterface $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTimeInterface $datePost;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getTitlePost(): ?string
    {
        return $this->titlePost;
    }

    public function setTitlePost(string $titlePost): self
    {
        $this->titlePost = $titlePost;

        return $this;
    }

    public function getJobDescription(): ?string
    {
        return $this->jobDescription;
    }

    public function setJobDescription(?string $jobDescription): self
    {
        $this->jobDescription = $jobDescription;

        return $this;
    }

    public function getMission(): ?string
    {
        return $this->mission;
    }

    public function setMission(?string $mission): self
    {
        $this->mission = $mission;

        return $this;
    }

    public function getSkills(): ?string
    {
        return $this->skills;
    }

    public function setSkills(string $skills): self
    {
        $this->skills = $skills;

        return $this;
    }

    public function getTechnos(): ?string
    {
        return $this->technos;
    }

    public function setTechnos(?string $technos): self
    {
        $this->technos = $technos;

        return $this;
    }

    public function getServices(): ?string
    {
        return $this->services;
    }

    public function setServices(?string $services): self
    {
        $this->services = $services;

        return $this;
    }

    public function getTools(): ?string
    {
        return $this->tools;
    }

    public function setTools(?string $tools): self
    {
        $this->tools = $tools;

        return $this;
    }

    public function getProfilRechercher(): ?string
    {
        return $this->profilRechercher;
    }

    public function setProfilRechercher(string $profilRechercher): self
    {
        $this->profilRechercher = $profilRechercher;

        return $this;
    }

    public function getInterview(): ?string
    {
        return $this->interview;
    }

    public function setInterview(?string $interview): self
    {
        $this->interview = $interview;

        return $this;
    }

    public function getContract(): ?string
    {
        return $this->contract;
    }

    public function setContract(string $contract): self
    {
        $this->contract = $contract;

        return $this;
    }

    public function getContractStartedAt(): ?\DateTimeInterface
    {
        return $this->contractStartedAt;
    }

    public function setContractStartedAt(\DateTimeInterface $contractStartedAt): self
    {
        $this->contractStartedAt = $contractStartedAt;

        return $this;
    }

    public function getContractEndedAt(): ?\DateTimeInterface
    {
        return $this->contractEndedAt;
    }

    public function setContractEndedAt(?\DateTimeInterface $contractEndedAt): self
    {
        $this->contractEndedAt = $contractEndedAt;

        return $this;
    }

    public function getLocalisation(): ?string
    {
        return $this->localisation;
    }

    public function setLocalisation(?string $localisation): self
    {
        $this->localisation = $localisation;

        return $this;
    }

    public function getWorkType(): ?string
    {
        return $this->workType;
    }

    public function setWorkType(string $workType): self
    {
        $this->workType = $workType;

        return $this;
    }

    public function getIsTransport(): ?bool
    {
        return $this->isTransport;
    }

    public function setIsTransport(?bool $isTransport): self
    {
        $this->isTransport = $isTransport;

        return $this;
    }

    public function getDiplomas(): ?string
    {
        return $this->diplomas;
    }

    public function setDiplomas(string $diplomas): self
    {
        $this->diplomas = $diplomas;

        return $this;
    }

    public function getSalary(): ?int
    {
        return $this->salary;
    }

    public function setSalary(?int $salary): self
    {
        $this->salary = $salary;

        return $this;
    }

    public function getExperience(): ?string
    {
        return $this->experience;
    }

    public function setExperience(string $experience): self
    {
        $this->experience = $experience;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getIsTreated(): ?bool
    {
        return $this->isTreated;
    }

    public function setIsTreated(?bool $isTreated): self
    {
        $this->isTreated = $isTreated;

        return $this;
    }

    public function getIsFromPortal(): ?bool
    {
        return $this->isFromPortal;
    }

    public function setIsFromPortal(bool $isFromPortal): self
    {
        $this->isFromPortal = $isFromPortal;

        return $this;
    }

    public function getIsFromEmploi(): ?bool
    {
        return $this->isFromEmploi;
    }

    public function setIsFromEmploi(bool $isFromEmploi): self
    {
        $this->isFromEmploi = $isFromEmploi;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getDatePost(): ?\DateTimeInterface
    {
        return $this->datePost;
    }

    public function setDatePost(\DateTimeInterface $datePost): self
    {
        $this->datePost = $datePost;

        return $this;
    }

    public function test()
    {
        $post = new Post();
    }
}
