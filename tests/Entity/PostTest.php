<?php

namespace Tests\Entity;

use ImkPostBundle\Entity\Post;
use PHPUnit\Framework\TestCase;

final class PostTest extends TestCase
{
    public function testEntityPost()
    {
        $post = new Post();
        $post->setContract("CDI");
        $this->assertSame("CDI", $post->getContract());
        $this->assertInstanceOf(Post::class, $post);
    }
}
